# Use Debian Buster image without python
FROM ubuntu:18.04

WORKDIR /usr/src/app

# required packages for pythonocc-core, which are otherwise not installed properly
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y


# Install Miniconda with python 3.8
ENV PATH="/root/miniconda3/bin:${PATH}"
ARG PATH="/root/miniconda3/bin:${PATH}"

#RUN apt-get update
RUN apt-get update -y \
    && apt-get install -y wget && rm -rf /var/lib/apt/lists/*
RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && mkdir /root/.conda \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh 

# Upgrade pip
RUN pip install --upgrade pip

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apt update
RUN apt install libopengl0 -y

# Install PythonOCC-Core using miniconda
RUN conda install -c conda-forge pythonocc-core=7.5.1 -y

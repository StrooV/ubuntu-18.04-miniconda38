# ubuntu-18.04-miniconda38

Ubuntu 18.04 with 
* Miniconda (python 3.8)
* PythonOCC-Core 7.5.1
* Prerequisites for MeshlabPy

Note that additional requirements still have to be installed. 
```
COPY ./requirements.txt .
RUN pip install psycopg2-binary
RUN pip install -r ./requirements.txt

WORKDIR /usr/src/app
COPY . .

EXPOSE 8000
```
## Pull ##
docker pull registry.gitlab.com/stroov/ubuntu-18.04-miniconda38

## Dockerfile ##
FROM registry.gitlab.com/stroov/ubuntu-18.04-miniconda38
...

## Docker Compose ##
...
  image: registry.gitlab.com/stroov/ubuntu-18.04-miniconda38
...
